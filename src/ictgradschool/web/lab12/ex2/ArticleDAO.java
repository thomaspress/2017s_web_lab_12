package ictgradschool.web.lab12.ex2;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by tpre939 on 4/01/2018.
 */
public class ArticleDAO implements AutoCloseable {
    private Connection conn;

    ArticleDAO(MySQLDatabase db) throws IOException,SQLException{
        this.conn = db.getConnection();
    }


    public List<Article> getBody(String title) {
        List<Article> articles = new ArrayList<>();
//        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM simpledao_articles WHERE title LIKE ?")) {
                stmt.setString(1, title);
                try (ResultSet r = stmt.executeQuery()) {
                    while (r.next()) {
                        Article thisArticle = new Article(r.getInt("artid"), r.getString("title"), r.getString("body"));
                        articles.add(thisArticle);
                    }

                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        return articles;
//        }
    }


    @Override
    public void close() throws SQLException {
        this.conn.close();
    }
}
