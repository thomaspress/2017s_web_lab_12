package ictgradschool.web.lab12.ex2;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by tpre939 on 4/01/2018.
 */
public class MySQLDatabase{
    Connection getConnection() throws IOException, SQLException{
        Properties dbProps = new Properties();
        try(FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps);
        return conn;

    }

//    public Connection getConnection() throws IOException, SQLException {
//
//        Properties dbProps = new Properties();
//        try (FileInputStream fis = new FileInputStream("connection.properties")) {
//            dbProps.load(fis);
//        }
//
//        Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps);
//        return conn;
//    }
}
