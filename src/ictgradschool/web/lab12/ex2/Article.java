package ictgradschool.web.lab12.ex2;

/**
 * Created by tpre939 on 4/01/2018.
 */
public class Article {
    private int id;
    private String title;
    private String body;

    public Article(){}

    public Article(int id, String title, String body){
        this.id = id;
        this.title = title;
        this.body = body;
    }

    public int getId(){
        return id;
    }
    public String getTitle(){
        return title;
    }
    public String getBody(){
        return body;
    }
}
