package ictgradschool.web.lab12.ex2;

import ictgradschool.web.lab12.Keyboard;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Exercise02 {
    public static void main(String[] args) throws IOException, SQLException{
        List<Article> articles = new ArrayList<>();
        ArticleDAO dao = new ArticleDAO( new MySQLDatabase());

        String title = "";

        while (title.equals("")) {
            System.out.println("Enter part or all of a article title: ");
            title = Keyboard.readInput();
            articles = dao.getBody(title);
            for (Article i: articles) {
                System.out.println(i.getBody());
            }
            title = "";

        }


    }
}
