package ictgradschool.web.lab12.ex1;

import ictgradschool.web.lab12.Keyboard;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Exercise01 {
    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        Properties dbProps = new Properties();

        try(FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String title = "";

        while (title.equals("")) {
            System.out.println("Enter part or all of a article title: ");
            title = Keyboard.readInput();

            // Set the database name to your database
            try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
                System.out.println("Connection successful");
                try (PreparedStatement stmt = conn.prepareStatement(
                        "SELECT * FROM simpledao_articles WHERE title LIKE ?"
                )) {
                    stmt.setString(1, title);
                    System.out.println(stmt);
                    try (ResultSet r = stmt.executeQuery()) {
                        while (r.next()) {
                            String body = r.getString("body");
                            System.out.println(body);
                        }
                        title = "";
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


    }
}
