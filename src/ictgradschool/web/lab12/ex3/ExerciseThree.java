package ictgradschool.web.lab12.ex3;

import ictgradschool.web.lab12.Keyboard;
import ictgradschool.web.lab12.ex3.generated.tables.PfilmsActor;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import static ictgradschool.web.lab12.ex3.generated.Tables.*;


/**
 * Created by tpre939 on 4/01/2018.
 */
public class ExerciseThree {
    private Properties dbProps = new Properties();

    public static void main(String[] args) throws SQLException {
        ExerciseThree ex3 = new ExerciseThree();
        System.out.println("Welcome to the Film database");
        System.out.println();
        ex3.start();

    }

    private void start() throws SQLException {
        try {
            try (FileInputStream fis = new FileInputStream("mysql.properties")) {
                dbProps.load(fis);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        String infoChoice = "";
        while ((!infoChoice.equals("1") && !infoChoice.equals("2")) && (!infoChoice.equals("3") && !infoChoice.equals("4"))) {
            System.out.println("Please select an option from the following:");
            System.out.println("1. Information by Actor");
            System.out.println("2. Information by Movie");
            System.out.println("3. Information by Genre");
            System.out.println("4. Exit");
            infoChoice = Keyboard.readInput();
            switch (infoChoice){
                case "1":
                    this.choice("actor");
                    infoChoice = "";
                    break;
                case "2":
                    this.choice("movie");
                    infoChoice = "";
                    break;
                case "3":
                    this.choice("genre");
                    infoChoice = "";
                    break;
                case "4": break;
            }
        }

    }

    private void choice (String s){
        String choice = "";
        while (choice.equals("")) {
            System.out.println("Please enter the name of the " + s + " you wish to get information about, or press enter to return to the previous menu");
            choice = Keyboard.readInput();
            if (!choice.equals("")) {
                switch (s){
                    case "actor":
                        choice = this.actorInfo(choice);
                        break;
                    case "movie":
                        choice = this.movieInfo(choice);
                        break;
                    case "genre":
                        choice = this.genreInfo(choice);
                        break;
                }

            } else break;
        }
    }


    private String actorInfo(String actorName){

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            DSLContext retrieve = DSL.using(conn, SQLDialect.MYSQL);
//            Result<Record> actor = retrieve.select().from(PFILMS_ACTOR).where(PFILMS_ACTOR.ACTOR_LNAME.eq(actorName).or(PFILMS_ACTOR.ACTOR_FNAME.eq(actorName))).fetch();
            //PFILMS_ACTOR.ACTOR_FNAME,PFILMS_ACTOR.ACTOR_LNAME,PFILMS_FILM.FILM_TITLE
            Result<Record> actor = retrieve.select()
                    .from(PFILMS_FILM,PFILMS_ACTOR,PFILMS_PARTICIPATES_IN)
                    .where(PFILMS_ACTOR.ACTOR_LNAME.eq(actorName))
                            .or(PFILMS_ACTOR.ACTOR_FNAME.eq(actorName))
                    .and(PFILMS_ACTOR.ACTOR_ID.eq(PFILMS_PARTICIPATES_IN.ACTOR_ID))
                    .and(PFILMS_FILM.FILM_ID.eq(PFILMS_PARTICIPATES_IN.FILM_ID))
                    .fetch();
            if (actor.isEmpty()) {
                System.out.println("Sorry, we couldn't find any actor by that name");
                return "";
            }else {
                String name;
                Record record = actor.get(0);
                name = record.getValue(PFILMS_ACTOR.ACTOR_FNAME) + " " + record.getValue(PFILMS_ACTOR.ACTOR_LNAME);
                System.out.println(name + " is listed as being involved in the following films: ");
                for (Record movies : actor){
                    String movie = movies.getValue(PFILMS_FILM.FILM_TITLE);
                    System.out.println(movie);
                }
            }
//            System.out.println(actor.toString());

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "success";
    }

    private String movieInfo(String filmName){

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            DSLContext retrieve = DSL.using(conn, SQLDialect.MYSQL);
//            Result<Record> actor = retrieve.select().from(PFILMS_ACTOR).where(PFILMS_ACTOR.ACTOR_LNAME.eq(actorName).or(PFILMS_ACTOR.ACTOR_FNAME.eq(actorName))).fetch();
            Result<Record> movie = retrieve.select().from(PFILMS_ACTOR,PFILMS_FILM,PFILMS_PARTICIPATES_IN,PFILMS_GENRE,PFILMS_ROLE)
                    .where(PFILMS_FILM.FILM_TITLE.eq(filmName))
                    .and(PFILMS_ACTOR.ACTOR_ID
                            .eq(PFILMS_PARTICIPATES_IN.ACTOR_ID))
                    .and(PFILMS_FILM.FILM_ID.eq(PFILMS_PARTICIPATES_IN.FILM_ID))
                    .and(PFILMS_FILM.GENRE_NAME.eq(PFILMS_GENRE.GENRE_NAME))
                    .and(PFILMS_ROLE.ROLE_ID.eq(PFILMS_PARTICIPATES_IN.ROLE_ID))
                    .fetch();
            if (movie.isEmpty()) {
                System.out.println("Sorry, we couldn't find any film by that name");
                return "";
            }else {
                Record record = movie.get(0);
                System.out.println("The film " + record.getValue(PFILMS_FILM.FILM_TITLE) + " is a " + record.getValue(PFILMS_FILM.GENRE_NAME) + " film that features the following people:");
                for (Record movies : movie){
                    String actors = movies.getValue(PFILMS_ACTOR.ACTOR_FNAME) + " " + movies.getValue(PFILMS_ACTOR.ACTOR_LNAME) + " (" + movies.getValue(PFILMS_ROLE.ROLE_NAME) + ")";
                    System.out.println(actors);
                }
            };

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "success";
    }

    private String genreInfo(String genreName){

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            DSLContext retrieve = DSL.using(conn, SQLDialect.MYSQL);
//            Result<Record> actor = retrieve.select().from(PFILMS_ACTOR).where(PFILMS_ACTOR.ACTOR_LNAME.eq(actorName).or(PFILMS_ACTOR.ACTOR_FNAME.eq(actorName))).fetch();
            Result<Record> genre = retrieve.select().from(PFILMS_FILM,PFILMS_GENRE)
                    .where(PFILMS_FILM.GENRE_NAME.eq(genreName))
                    .and(PFILMS_FILM.GENRE_NAME.eq(PFILMS_GENRE.GENRE_NAME))
                    .fetch();
            if (genre.isEmpty()) {
                System.out.println("Sorry, we couldn't find any genre by that name");
                return "";
            }else {
                String name;
                Record record = genre.get(0);
                name = record.getValue(PFILMS_FILM.GENRE_NAME);
                System.out.println("The " + name + " genre contains the following films: ");
                for (Record movies : genre){
                    String movie = movies.getValue(PFILMS_FILM.FILM_TITLE);
                    System.out.println(movie);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "success";
    }
}
